# ZPO-mini-projects

Miniproject on topics:

1. removing noise
2. convolution and geometric transformation
3. high and low pass filter using DFT
4. white noise, Gauss filter, and its separability
5. Salt-and-pepper noise, a median filter
